-- 11.�������� ������������� �����. �������� � ����: ��������, ���������, ����.
-- ������� ��������������� ���������, ������� �����������, ��������� ��������,
-- ������� ����� (��� ���������) � ������, ��������� ������ ���� ��������� ���
-- ��������� ������� � ����������� ���� ���������� � ���. ���������
-- ���������������: ��������� ������ (� ������ ������ ����� ���� ���������
-- ���������), ���������, ��������� � ����� � ������. ��� ���������������:
-- ��������� ��������, ����� �������, ����������� ���� ����, ���������������
-- ��������, �������������� ���������.
-- a. ���� � �� �� ��������� �� ����� ���� ������� � ����������� ��������.
-- b. ���� � �� �� ��������� ����� ���� ������� � ����������� ��������.


CREATE TABLE `Excurs` (
`City` varchar(20) NOT NULL,
`Name` text NOT NULL,
`description` varchar(30) NOT NULL,
`price` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8
CREATE TABLE `rout` (
`Name` varchar(20) NOT NULL,
`citystart` text NOT NULL,
`description` varchar(20) NOT NULL,
`price` int(11) NOT NULL,
PRIMARY KEY (`Name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8
CREATE TABLE `rout_citys` (
`Rout` varchar(20) NOT NULL,
`City` varchar(20) NOT NULL,
`Days` int(11) NOT NULL,
KEY `Rout` (`Rout`),
CONSTRAINT `rout_citys_ibfk_1` FOREIGN KEY (`Rout`) REFERENCES `rout` (`Name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8
CREATE TABLE `tour` (
`route` varchar(20) NOT NULL,
`date` date NOT NULL,
`days` int(2) NOT NULL,
`price` int(10) NOT NULL,
`description` text NOT NULL,
KEY `route` (`route`),
CONSTRAINT `tour_ibfk_1` FOREIGN KEY (`route`) REFERENCES `rout` (`Name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8




INSERT INTO `Excurs` (`City`, `Name`, `description`, `price`) VALUES ('�����', '������ � �����', '�����', '1487');
INSERT INTO `Excurs` (`City`, `Name`, `description`, `price`) VALUES ('������', '������ � ����� �����', '������', '3000');
INSERT INTO `Excurs` (`City`, `Name`, `description`, `price`) VALUES ('�����', '������', 'test', '3220');


INSERT INTO `rout` (`Name`, `citystart`, `description`, `price`) VALUES ('�����', '������', '�����-������', '7500');
INSERT INTO `rout` (`Name`, `citystart`, `description`, `price`) VALUES ('�����-�����', '�����', '�����', '0');
INSERT INTO `rout` (`Name`, `citystart`, `description`, `price`) VALUES ('������', '������', '��� �� ������', '0');


INSERT INTO `rout_citys` (`Rout`, `City`, `Days`) VALUES ('�����', '�����', '3');
INSERT INTO `rout_citys` (`Rout`, `City`, `Days`) VALUES ('�����', '������', '2');

INSERT INTO `tour` (`route`, `date`, `days`, `price`, `description`) VALUES ('�����', '2019-12-03', '13', '13377', '������ � �����������');
INSERT INTO `tour` (`route`, `date`, `days`, `price`, `description`) VALUES ('������', '2019-12-10', '13', '0', '��������� ��-��-��');
INSERT INTO `tour` (`route`, `date`, `days`, `price`, `description`) VALUES ('�����', '2019-12-09', '13', '0', '����');